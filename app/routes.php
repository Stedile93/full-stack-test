<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showHome');


Route::get('user/login', 'UsersController@showLogin');

Route::post('user/login', 'UsersController@doLogin');
Route::post('user/register', 'UsersController@create');

Route::get('user/logout', 'UsersController@doLogout');

Route::filter('auth', function(){
	if (Auth::guest()) return Redirect::guest('user/login');
});

// Private pages
Route::group(array('before' => 'auth'), function(){

	Route::get('/post/create', 'PostsController@showCreate');
	Route::post('/post/create', 'PostsController@create');

	Route::get('/post/vote/{id}', 'PostsController@countVote');

});
