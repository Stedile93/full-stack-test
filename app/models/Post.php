<?php

use Carbon\Carbon;

class Post extends Eloquent {

	protected $table = 'posts';
	
	public function user(){
        return $this->belongsTo('User', 'id_user');
    }
    
    public function formatDate($date){
        return Carbon::parse($date)->format('d/m/Y \à\s H:i:s');
    }

}
