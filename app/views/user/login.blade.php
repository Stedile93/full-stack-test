@extends('master')


@section('content')
    
<div class="row">
    
    <div class="col-md-4 pull-left">
        
        {{ Form::open(array('url' => 'user/login', 'id' => 'form-login')) }}
            <h2 class="form-signup-heading">Login</h2>
         
            <ul>
                @if(Session::has('form-send') && Session::get('form-send') == 'login')
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                @endif
            </ul>
    
            <p class="form-group">
                {{ Form::text('username', Input::old('username'), array('class'=>'form-control', 'placeholder'=>'Your username', 'required')) }}
            </p>
    
            <p class="form-group">
                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Your password', 'required')) }}
            </p>
    
            <p>{{ Form::submit('Entrar', array('class'=>'btn btn-large btn-primary btn-block')) }}</p>
        {{ Form::close() }}
        
    </div>
    
    <div class="col-md-4 pull-right">
        
        {{ Form::open(array('url'=>'user/register', 'class'=>'form-signup')) }}
            <h2 class="form-signup-heading">Register</h2>
         
            <ul>
                @if(Session::has('form-send') && Session::get('form-send') == 'register')
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                @endif
            </ul>
         
            <p class="form-group">
                {{ Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'Full Name', 'required')) }}
            </p>
            <p class="form-group">
                {{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Username', 'required')) }}
            </p>
            <p class="form-group">
                {{ Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address', 'required')) }}
            </p>
            <p class="form-group">
                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password', 'required')) }}
            </p>
            <p class="form-group">
                {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password', 'required')) }}
            </p>
         
            {{ Form::submit('Register', array('class'=>'btn btn-large btn-primary btn-block'))}}
        {{ Form::close() }}
        
    </div>
    
</div>

@stop
