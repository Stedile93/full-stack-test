@extends('master')


@section('content')


@if($posts->count() > 0)
    <a class="btn btn-success btn-lg pull-right btn-create" href="{{URL::to('post/create')}}" role="button">Create Lighting Talk</a>

    @foreach($posts as $post)
        <div class="row post">
            <div class="col-md-12">
                <h2>{{$post->title}}</h2>
                <p>
                    {{$post->description}}
                </p>
                
                    
                <small class="pull-left">posted by {{$post->user->username}} | {{ $post->formatDate($post->created_at) }} | {{$post->votes}} votes</small>
                
                @if(Auth::check())
                    <a class="btn btn-default pull-right btn-xs" id="btn-votar" href="{{URL::to('post/vote/'.$post->id)}}" role="button">Vote</a>
                @else
                    <a class="btn btn-default pull-right btn-xs" href="{{URL::to('user/login')}}" role="button">Login to vote</a>
                @endif
            </div>
        </div>
    @endforeach

@else

    <div class="row">
        <div class="col-md-12">
            <h4>Be the first! Make login and submit your post</h4>
            <p>
                @if(Auth::check())
                    <a class="btn btn-default" id="btn-votar" href="{{URL::to('post/create')}}" role="button">Create Lighting Talk</a>
                @else
                    <a class="btn btn-default" href="{{ URL::to('user/login') }}" role="button">Login / Register</a>
                @endif
            </p>
        </div>
    </div>
        
@endif

@stop
