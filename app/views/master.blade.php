<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Giuliano Stedile">

        <title>Lighting Talks Polling</title>

        <!-- O atributo final "true" nas chamadas de css e javascript é adicionado para que o
        link com https funcione normalmente, necessário para o desenvolvimento
        a partir do cloud9 (IDE online) -->

        {{ HTML::style('node_modules/bootstrap/dist/css/bootstrap.min.css', array('type' => 'text/css')) }}
        {{ HTML::style('assets/css/main.css', array('type' => 'text/css')) }}

        <!-- custom css of pages -->
        @yield('css')

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <header class="navbar navbar-inverse barra-topo">
            <nav class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ URL::to('/') }}">Lighting Talks Pooling</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="menu-direita pull-right">
                        @if(Auth::check())
                            <li>Hello, {{Auth::user()->name}}</li>
                            <li><a href="{{ URL::to('user/logout') }}">Logout</a></li>
                        @else
                            <li><a href="{{ URL::to('user/login') }}">Login</a></li>
                        @endif
                    </ul>
                </div>
            </nav>
        </header>
        
        <main class="container">
            @if(Session::has('message'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Ops!</strong> {{ Session::get('message') }}
                </div>
            @endif
        
            @yield('content')
        </main>
        
        <footer class="container">
            <hr>

            <p>&copy; 2016 - Lighting Talks Pooling</p>
        </footer>


        {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', array('type' => 'text/javascript')) }}

        {{ HTML::script('node_modules/bootstrap/dist/js/bootstrap.min.js', array('type' => 'text/javascript')) }}

        <!-- custom javascripts of pages -->
        @yield('js')


    </body>
</html>
