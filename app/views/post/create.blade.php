@extends('master')


@section('content')
    
<div class="row">

    {{ Form::open(array('url' => 'post/create', 'id' => 'form-cadastro', 'class' => 'col-md-12')) }}
        <h1>Cadastro de Post</h1>

        <p class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', Input::old('title'), array('class' => 'form-control', 'required')) }}
        </p>

        <p class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', Input::old('description'), array('class' => 'form-control', 'required')) }}
        </p>

        <p>{{ Form::submit('Cadastrar', array('class' => 'btn btn-large btn-primary')) }}</p>
    {{ Form::close() }}

</div>

@stop
