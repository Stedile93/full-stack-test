<?php

class UserTableSeeder extends Seeder{

    public function run(){
        DB::table('users')->delete();
        User::create(array(
            'name'     => 'Giuliano Stedile',
            'email'    => 'giu.stedile@gmail.com',
            'username' => 'stedile93',
            'password' => Hash::make('teste123'),
        ));
    }

}
