<?php

class HomeController extends BaseController {

	/**
     * Show home page.
     *
     * @return Response
     */
	public function showHome(){

		$posts = Post::orderBy('created_at', 'desc')->get();


		$dadosView = array(
			'posts' => $posts
		);

		return View::make('home')->with($dadosView);
	}

}
