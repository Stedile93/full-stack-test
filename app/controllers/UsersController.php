<?php

class UsersController extends BaseController {
	
	

	/**
     * Show the page of login form.
     *
     * @return Response
     */
	public function showLogin(){
		return View::make('user.login');
	}

	/**
     * Show the page of login form.
     *
     * @return Response
     */
	public function create(){
		
		$validator = Validator::make(Input::all(), User::$rules);
 
	    if ($validator->passes()) {
	    	
	        $user = new User();
			$user->name     = Input::get('name');
			$user->email    = Input::get('email');
			$user->username = Input::get('username');
	
			if(Str::length(Input::get('password')) < 6){
				return Response::json(['status' => 0, 'msg' => 'Sua senha precisa ter no mínimo 6 caracteres!']);
			}
	
			$user->password = Hash::make(Input::get('password'));
	
			if($user->save()){
				// Faz o login do cliente ao efetuar cadastro
				Auth::attempt(array('username' => $user->username, 'password' => Input::get('password')), true);
	
				return Redirect::to('/');
			}else{
				return Redirect::to('user/login')->with(array('form-send' => 'register', 'message' => 'Something wrong are happen, we`ll work hard to resolve.'))->withInput();
			}
	    } else {
	        return Redirect::to('user/login')->with(array('form-send' => 'register', 'message' => 'The following errors occurred'))->withErrors($validator)->withInput();
	    }
	    
	}
	
	/**
     * Get infos and do login.
     *
     * @return Response
     */
	public function doLogin(){
		
		
		$rulesLogin = array(
			'username'              =>'required|alpha_num',
			'password'              =>'required|alpha_num|between:6,12'
	    );
	    
	    $validator = Validator::make(Input::all(), $rulesLogin);
 
	    if ($validator->passes()) {
			// Auth
			if(Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')), true)){
				return Redirect::to('/');
			}else{
				return Redirect::to('user/login')->with(array('form-send' => 'login', 'message' => 'The following errors occurred'))->withErrors($validator)->withInput();
			}
	    }else{
	    	return Redirect::to('user/login')->with(array('form-send' => 'login', 'message' => 'The following errors occurred'))->withErrors($validator)->withInput();
	    }
	}

	/**
     * Do logout and redirect to home.
     *
     * @return Response
     */
	public function doLogout(){
		Auth::logout();
		return Redirect::to('/');
	}

}
