<?php

class PostsController extends BaseController {

	/**
     * Show the page of create form
     *
     * @return Response
     */
	public function showCreate(){
		return View::make('post.create');
	}

	/**
     * Show the page of post form.
     *
     * @return Response
     */
	public function create(){
		
		$rules = array(
			'title'       =>'required|alpha_num',
			'description' =>'required|alpha_num'
	    );
	    
	    $validator = Validator::make(Input::all(), $rules);
 
	    if ($validator->passes()) {

			$post = new Post();
			$post->id_user        = Auth::user()->id;
			$post->title          = Input::get('title');
			$post->description    = Input::get('description');
	
			if($post->save()){
				return Redirect::to('/');
			}else{
				return Redirect::to('post/create')->with(array('message' => 'The following errors occurred'))->withErrors($validator)->withInput();
			}
	    }else{
	    	return Redirect::to('post/create')->with(array('message' => 'The following errors occurred'))->withErrors($validator)->withInput();
	    }
	}

	/**
     * Count a vote for post
     *
     * @return Response
     */
	public function countVote($id){

		$post = Post::find($id);
		$post->votes = $post->votes + 1;

		$post->save();

		return Redirect::to('/');
	}

}
